The Discovery House is the best drug rehab center in Los Angeles, California. We offer treatment for individuals struggling with alcohol addiction, drug addiction, or both. We pride ourselves on the scope of our programs from sub-acute detox and residential treatment to intensive outpatient programs.

Address: 7133A Darby Ave, Reseda, CA 91335, USA

Phone: 818-698-8560
